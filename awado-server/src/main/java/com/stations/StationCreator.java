package com.stations;


import java.util.ArrayList;

public class StationCreator {

    public ArrayList<EbikeStation> stations = new ArrayList<EbikeStation>();

    public StationCreator(){}

    public void Create(){
        this.stations.add(new EbikeStation(0, 25, 5, 20, 48.783536, 9.181004, "Hauptbahnhof"));
        this.stations.add(new EbikeStation(1, 25, 25, 0, 48.775920, 9.182774, "CharlottenPlatz"));
        this.stations.add(new EbikeStation(2, 25, 6, 19, 48.778509, 9.167472, "Berliner Platz"));
        this.stations.add(new EbikeStation(3, 25, 24, 1, 48.764495, 9.168043, "Marienplatz"));
        this.stations.add(new EbikeStation(4, 25, 16, 9, 48.773312, 9.180124, "Rathaus"));
        this.stations.add(new EbikeStation(5, 25, 23, 2, 48.775136, 9.172045, "Stadtmitte"));
        this.stations.add(new EbikeStation(6, 25, 4, 21, 48.781048, 9.173247, "HFT"));
        this.stations.add(new EbikeStation(7, 25, 14, 11, 48.775207, 9.154900, "Burgerburo west"));
        this.stations.add(new EbikeStation(8, 25, 16, 9, 48.786299, 9.190220, "Neckartor"));
        this.stations.add(new EbikeStation(9, 25, 3, 22, 48.787535, 9.151403, "Lenzhalde"));
        this.stations.add(new EbikeStation(10, 25, 21, 4, 48.727019, 9.112468, "Vaihingen"));
        this.stations.add(new EbikeStation(11, 25, 12, 13, 48.741483, 9.095345, "Hochschusport Stuttgart"));
        this.stations.add(new EbikeStation(12, 25, 17, 8, 48.791957, 9.212815, "CottaSchule"));
        this.stations.add(new EbikeStation(13, 25, 21, 4, 48.781233, 9.207418, "Wagenburgstrasse"));
        this.stations.add(new EbikeStation(14, 25, 10, 15, 48.790905, 9.180816, "Stadtbibliothek"));
        this.stations.add(new EbikeStation(15, 25, 4, 21, 48.793467, 9.225894, "NeckarPark"));
        this.stations.add(new EbikeStation(16, 25, 7, 18, 48.803141, 9.216273, "Wilhelmsplatz"));
        this.stations.add(new EbikeStation(17, 25, 3, 22, 48.817697, 9.204225, "Hallschlag"));
        this.stations.add(new EbikeStation(18, 25, 22, 3, 48.807060, 9.188845, "Lowentor"));
        this.stations.add(new EbikeStation(19, 25, 2, 23, 48.779992, 9.249125, "Unterturlheim"));
        this.stations.add(new EbikeStation(20, 25, 15, 10, 48.773745, 9.194279, "Gansheide"));
        this.stations.add(new EbikeStation(21, 25, 1, 24, 48.769821, 9.185728, "DobelStrasse"));
        this.stations.add(new EbikeStation(22, 25, 11, 14, 48.781800, 9.158431, "Holderlinplatz"));
        this.stations.add(new EbikeStation(23, 25, 18, 7, 48.770630, 9.172777, "Das Gerber"));
        this.stations.add(new EbikeStation(24, 25, 23, 2, 48.774844, 9.145912, "Vogelsang"));
    }
}
