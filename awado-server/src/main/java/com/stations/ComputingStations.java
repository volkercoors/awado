package com.stations;

import java.util.ArrayList;

public class ComputingStations {

    private static final double R = 6372.8;

    public static double RetrieveDistance(double lat1, double lon1,
                                  double lat2, double lon2) {

        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(dLat / 2), 2) + Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    }

    public EbikeStation CheckForClosestCheckpoint(double lat, double lng, StationCreator info){
        info.Create();

        for (int i = 0; i < info.stations.size(); i++){

            double dist = RetrieveDistance(info.stations.get(i).getLat(), info.stations.get(i).getLng(), lat, lng);
            System.out.println("Bike Id is: " + info.stations.get(i).getId() + " and dist is: " + dist);
            info.stations.get(i).setDistance(dist);

        }

        return FindShortestStation(info.stations);
    }

    public int sortList(ArrayList<InfoStations> infos){

        int index = -1;
        double tempDist = 0;

        for(int i = 0; i < infos.size(); i++){
            if (infos.get(i).getDistance() > tempDist){
                tempDist = infos.get(i).getDistance();
                index = i;
            }
        }

        return index;
    }

    public EbikeStation FindShortestStation(ArrayList<EbikeStation> bikes){
        double distance = -1;
        int id = -1;
        for (int i = 0; i < bikes.size(); i++){
            if (distance == -1 || distance > bikes.get(i).getDistance()){
                distance = bikes.get(i).getDistance();
                id = bikes.get(i).getId();
            }
        }

        System.out.println(id);
        return bikes.get(id);
    }

    public static void main(String[] args){
        StationCreator test = new StationCreator();
        ComputingStations cp = new ComputingStations();

        //Ici en retour de la fonction CheckForClosestStation tu récupéres un objet EbikeStation si tu
        //as des questions sur la classe regarde le fichier EbikeStation.java
        cp.CheckForClosestCheckpoint(48.771264, 9.173164, test);

    }
}
