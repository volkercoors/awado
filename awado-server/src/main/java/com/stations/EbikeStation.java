package com.stations;

public class EbikeStation {

    private int id;
    private int capacity;
    private int available;
    private int freeSpace;
    private double lat;
    private double lng;
    private String name;
    private double distance;

    public EbikeStation(){}

    public EbikeStation(int id, int capacity, int available, int freeSpace, double lat, double lng, String name){

        this.id = id;
        this.capacity = capacity;
        this.available = available;
        this.freeSpace = freeSpace;
        this.lat = lat;
        this.lng = lng;
        this.name = name;
        this.distance = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getFreeSpace() {
        return freeSpace;
    }

    public void setFreeSpace(int freeSpace) {
        this.freeSpace = freeSpace;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
