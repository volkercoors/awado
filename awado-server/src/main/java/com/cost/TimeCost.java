package com.cost;

public class TimeCost {
    public double CalculationOfCost(int seconds, double price,int FreeSeconds){
        double cost = 0;

        if(seconds>=FreeSeconds)
            cost = (seconds-FreeSeconds)*(price/3600);

        return cost;
    }
}
