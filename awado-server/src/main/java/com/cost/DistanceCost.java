package com.cost;

public class DistanceCost {

    // FreeDistance = 5000
    // price = 0.1
    public double CalculationOfCost(int distance, int FreeDistance, double price){
        double cost = 0;

        if (distance >= FreeDistance){
            cost = ((double)(distance - FreeDistance)/1000) * price;
        }

        return cost;
    }
}
