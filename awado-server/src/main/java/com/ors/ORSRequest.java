package com.ors;

public class ORSRequest {

	// coordinates
	String coordinates;

	// apikeys
	String apiKey;

	// static parameters
	public static String FORMAT = "json";
	public static String UNITS = "m";
	public static String GEOMETRY_FORMAT = "polyline";
	public static String GEOMETRY_SIMPLIFY = "FALSE";
	public static String LANGUAGE = "EN";

	// profile related
	String profile;
	String preferences;
	String options;

	Boolean multiplePointsExists;
	Boolean poiNeeded;
	Boolean timeLimitNeeded;
}
