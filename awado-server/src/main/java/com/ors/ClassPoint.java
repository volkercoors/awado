package com.ors;

public class ClassPoint {
	Double latitude;
	Double longitute;

	public ClassPoint(Double latitude, Double longitute) {
		this.latitude = latitude;
		this.longitute = longitute;
	}
	ClassPoint() {}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitute() {
		return longitute;
	}

	public void setLongitute(Double longitute) {
		this.longitute = longitute;
	}
}