package com.ors;

import org.codehaus.jettison.json.JSONObject;

//import org.json.JSONObject;
public class ORSResponse {

	/** responseCode:200*/
	public static String SUCCESS = "success";
	/** responseCode:/400/500/etc*/
	public static String FAILURE = "failure";
	/** By default cycling-safe is checked, so this flag is set to true*/
	public boolean safeRouteBoolean = true;

	Integer responseCode;
	String responseMessage;
	JSONObject responseJSON;

	public Integer getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public JSONObject getResponseJSON() {
		return responseJSON;
	}

	public void setResponseJSON(JSONObject responseJSON) {
		this.responseJSON = responseJSON;
	}
	public boolean isSafeRouteBoolean() { return this.safeRouteBoolean;	}
	public void setSafeRouteBoolean(boolean safeRouteBoolean) {	this.safeRouteBoolean = safeRouteBoolean;}

	public ORSResponse(Integer responseCode, String responseMessage, JSONObject responseJSON) {
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
		this.responseJSON = responseJSON;
	}
	public ORSResponse(){};
}
