package com.ors;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

//import org.json.JSONException;
//import org.json.JSONObject;
public class ORSHandler {

	public static ORSResponse handleORSProcess(ClassPoint startPoint, ClassPoint endPoint, String preference,String profile)
			throws IOException, JSONException {

		ORSRequest orsRequest = new ORSRequest();

		// prepare coordinates string
		String startPointString = startPoint.longitute + "," + startPoint.latitude;
		String endPointString = endPoint.longitute + "," + endPoint.latitude;

		// service parameters
		orsRequest.coordinates = startPointString + "|" + endPointString;
		orsRequest.apiKey = "5b3ce3597851110001cf6248f1dd3fd0a03b471993f4834d0c90b616";
		orsRequest.profile = profile;
		orsRequest.preferences = preference;
		orsRequest.options = "Steepness_difficulty:0";

		return makeORSCall(orsRequest);
	}

	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);

			JSONObject jsonObj = new JSONObject(jsonText);
			return jsonObj;
		} finally {
			is.close();
		}
	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static ORSResponse makeORSCall(ORSRequest orsRequest) throws IOException, JSONException {
		String orsURLString = "https://api.openrouteservice.org/directions?api_key=" + orsRequest.apiKey + "&"
				+ "coordinates=" + orsRequest.coordinates + "&" + "profile=" + orsRequest.profile + "&"
				+ "format=" + orsRequest.FORMAT + "&geometry_format="
				+ orsRequest.GEOMETRY_FORMAT + "&" + "preference=" + orsRequest.preferences + "&geometry=true&elevation=true&extra_info=steepness"

//				+ "options={" + orsRequest.options + "}
				;
		// String orsAPIUrl =
		// "https://api.openrouteservice.org/directions?api_key=5b3ce3597851110001cf6248f1dd3fd0a03b471993f4834d0c90b616&coordinates=8.34234,48.23424%7C8.34423,48.26424&profile=driving-car&format=json&geometry_format=polyline";
		ORSResponse serviceResponse;
		
		System.out.println("orsURLString "+orsURLString);
		URL orsURL = new URL(orsURLString);

		HttpURLConnection con = (HttpURLConnection) orsURL.openConnection();
		JSONObject json = null;
		try {
			json = readJsonFromUrl(orsURLString);
		}
		catch(IOException e )
		{
			serviceResponse = null;
		}
		

		int status = con.getResponseCode();
		System.out.println("status " + status);
		if (json == null || status != 200) {
			serviceResponse = new ORSResponse(status, ORSResponse.FAILURE, json);
		} else {
			serviceResponse = new ORSResponse(status, ORSResponse.SUCCESS, json);
		}

		System.out.println("orsAPIUrl " + orsURLString);
		System.out.println("json " + serviceResponse.getResponseJSON());

		return serviceResponse;
	}

	public static void main(String[] args) {
		ClassPoint startPoint = new ClassPoint();
		startPoint.latitude = 8.34234;
		startPoint.longitute = 48.23424;

		ClassPoint endPoint = new ClassPoint();
		endPoint.latitude = 8.34423;
		endPoint.longitute = 48.26424;

		try {
			ORSResponse orsServiceResponse = handleORSProcess(startPoint, endPoint, "shortest", "cycling-electric");
		} catch (IOException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
