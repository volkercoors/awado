package com.api.routes.getCoordinates;

import com.bikes.BikeDetails;
import com.bikes.FloatingBike;
import com.cost.DistanceCost;
import com.ors.ClassPoint;
import com.ors.ORSHandler;
import com.ors.ORSResponse;
import com.stations.ComputingStations;
import com.stations.EbikeStation;
import com.stations.StationCreator;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/getCoordinates")
public class GetCoordinates {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getMessage() {
        return Response.ok("SALUT")
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, con" +
                        "tent-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD")
                .build();
    }


    @OPTIONS
    public Response getRequest2() {
        return Response.ok()
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, con" +
                        "tent-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD")
                .build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getRequest(JSONObject object) throws JSONException, IOException {

        // TODO: calculate bike position using bike id
        Map<String, ORSResponse> preferenceResponseMap = new HashMap();
        List<String> preferenceList = Arrays.asList("shortest", "fastest", "recommended");
       // TODO: pass bike latitude and longitude as the start point

        JSONObject startPointJson = object.getJSONObject("startpoint");
        Double startLat = startPointJson.getDouble("lat");
        Double startLng = startPointJson.getDouble("lng");
        String ebikeID = object.getString("ebikeID");
        JSONObject endPointJson = object.getJSONObject("endpoint");
        Double endLat = endPointJson.getDouble("lat");
        Double endLng = endPointJson.getDouble("lng");
        System.out.println("ebikeID: " + ebikeID);
        JSONObject finalRouteJson = new JSONObject();
        if (ebikeID.equals("-1")) {
            ClassPoint startPoint = new ClassPoint(startLat, startLng);
            ClassPoint endPoint = new ClassPoint(endLat, endLng);
            ORSResponse startPointToBikeJson = ORSHandler.handleORSProcess(startPoint, endPoint, "shortest", "foot-walking");
            JSONArray routesJsonArray1 = startPointToBikeJson.getResponseJSON().getJSONArray("routes");
            JSONObject routesJsonObj1 = routesJsonArray1.getJSONObject(0);


            JSONArray finalRouteJsonArr = new JSONArray();
            JSONArray routeJsonArray = new JSONArray();

            JSONObject summaryJsonObj1 = routesJsonObj1.getJSONObject("summary");
            summaryJsonObj1.put("preference", "shortest");
            summaryJsonObj1.put("profile", "foot-walking");
            summaryJsonObj1.put("price", 0.0);

            JSONArray geometryJsonArray1 = routesJsonObj1.getJSONArray("geometry");
            JSONObject fullRouteJson = new JSONObject();
            JSONObject finalGeometryJson = new JSONObject();

            finalGeometryJson.put("walkingStart",geometryJsonArray1);
            fullRouteJson.put("summary", summaryJsonObj1)
                    .put("geometry", finalGeometryJson);



            JSONObject fullrouteObj = new JSONObject();
            fullrouteObj.put("route",fullRouteJson );
            routeJsonArray.put(fullrouteObj);
            JSONArray bbox = new JSONArray();

            bbox = routesJsonObj1.getJSONArray("bbox");
            finalRouteJson.put("routes", routeJsonArray);
            finalRouteJson.put("bbox", bbox);


        } else {
            FloatingBike floatingBike = new FloatingBike();
            BikeDetails bikeDetails = new BikeDetails();
            bikeDetails.create();
            for (int j = 0; j < bikeDetails.ebikes.size(); j++) {
                if (bikeDetails.ebikes.get(j).geteBikeId().equals(ebikeID)) {
                    floatingBike = bikeDetails.ebikes.get(j);
                }
            }
            System.out.println(floatingBike.geteBikeId());
            System.out.println(floatingBike.getLng());
            System.out.println(floatingBike.getLat());
            System.out.println(floatingBike.getBatteryLevel());
            ClassPoint ebikeStartPoint =  new ClassPoint(floatingBike.getLat(),floatingBike.getLng());

            ClassPoint startPoint = new ClassPoint(startLat, startLng);
            ClassPoint endPoint = new ClassPoint(endLat, endLng);

            ORSResponse startPointToBikeJson = ORSHandler.handleORSProcess(startPoint, ebikeStartPoint, "shortest", "foot-walking");

            JSONArray routesJsonArray1 = startPointToBikeJson.getResponseJSON().getJSONArray("routes");
            JSONObject routesJsonObj1 = routesJsonArray1.getJSONObject(0);


            JSONObject summaryJsonObj1 = routesJsonObj1.getJSONObject("summary");
            JSONArray geometryJsonArray1 = routesJsonObj1.getJSONArray("geometry");
            //SONArray bbox = routesJsonObj1.getJSONArray("bbox");
            JSONArray bbox = new JSONArray();
            bbox.put(routesJsonObj1.getJSONArray("bbox").get(0));
            bbox.put(routesJsonObj1.getJSONArray("bbox").get(1));
            for (String preference : preferenceList) {
                ORSResponse orsResponseJson = ORSHandler.handleORSProcess(ebikeStartPoint, endPoint, preference,"cycling-regular" );
                preferenceResponseMap.put(preference, orsResponseJson);
            }



            Map<Integer, JSONObject> routeJsonMap = new HashMap();
            Integer i = 1;
            JSONObject routesJsonObj2 = null;
            for (Map.Entry<String, ORSResponse> preferenceResponseEntry : preferenceResponseMap.entrySet()) {
                ORSResponse orsResponseJson = preferenceResponseEntry.getValue();

                JSONArray routesJsonArray2 = orsResponseJson.getResponseJSON().getJSONArray("routes");
                routesJsonObj2 = routesJsonArray2.getJSONObject(0);

                //JSONArray summaryJsonArray2 = routesJsonObj2.getJSONArray("summary");
                JSONObject summaryJsonObj2 = routesJsonObj2.getJSONObject("summary");
                //add duration and distance of walking and ebike
                Double distance = summaryJsonObj1.getDouble("distance") +  summaryJsonObj2.getDouble("distance");
                Double duration =  summaryJsonObj1.getDouble("duration") +  summaryJsonObj2.getDouble("duration");

                JSONArray geometryJsonArray2 = routesJsonObj2.getJSONArray("geometry");

                JSONObject steepnessJsonObj3 = routesJsonObj2.getJSONObject("extras").getJSONObject("steepness");

                Double priceDistance = summaryJsonObj2.getDouble("distance");
                JSONObject finalSummaryJson = new JSONObject();
                DistanceCost distanceCost = new DistanceCost();
                finalSummaryJson.put("distance", distance)
                        .put("duration", duration)
                        .put("preference", preferenceResponseEntry.getKey())
                        .put("profile", "cycling-regular")
                        .put("ascent", summaryJsonObj2.getDouble("ascent"))
                        .put("descent",  summaryJsonObj2.getDouble("descent"))
                        .put("safe", false)
                        .put("price", distanceCost.CalculationOfCost(priceDistance.intValue(), 5000, 0.1));

                JSONObject finalGeometryJson = new JSONObject();

                finalGeometryJson.put("walkingStart",geometryJsonArray1)
                        .put("cycling",geometryJsonArray2);

                JSONObject finalsteepnessJson = new JSONObject();
                finalsteepnessJson.put("summary", steepnessJsonObj3.getJSONArray("summary"));

                JSONObject routeJson = new JSONObject();
                routeJson.put("summary", finalSummaryJson)
                        .put("geometry", finalGeometryJson)
                        .put("steepness", finalsteepnessJson);

                routeJsonMap.put(i,routeJson);
                i++;
            }
            bbox.put(routesJsonObj2.getJSONArray("bbox").get(2));
            bbox.put(routesJsonObj2.getJSONArray("bbox").get(3));

            JSONArray routeJsonArray = new JSONArray();

            // for (Map.Entry<Integer, JSONObject> routeJsonMapEntry : routeJsonMap.entrySet()) {
            for(JSONObject routeJsonObj :routeJsonMap.values() )  {
                JSONObject routeObj = new JSONObject();
                routeObj.put("route",routeJsonObj );
                routeJsonArray.put(routeObj);
            }
            //TODO Call Nearest ebikeStation algorithm

            StationCreator stationCreator = new StationCreator();
            ComputingStations cp = new ComputingStations();

            stationCreator.Create();
            EbikeStation ebikeStation = new EbikeStation();
            ebikeStation = cp.CheckForClosestCheckpoint(endPoint.getLatitude(), endPoint.getLongitute(), stationCreator);
            System.out.println("Station lat: " + ebikeStation.getLat());
            System.out.println("Station lng: " + ebikeStation.getLng());
            ClassPoint eBikeStationPoint = new ClassPoint(ebikeStation.getLat(),ebikeStation.getLng());


            ORSResponse startpointToEbikeStation = ORSHandler.handleORSProcess(ebikeStartPoint, eBikeStationPoint, "shortest", "cycling-regular");
            ORSResponse ebikeStationToEndpoint = ORSHandler.handleORSProcess(eBikeStationPoint, endPoint, "shortest", "foot-walking");

            JSONArray routesJsonArray3 = startpointToEbikeStation.getResponseJSON().getJSONArray("routes");
            JSONObject routesJsonObj3 = routesJsonArray3.getJSONObject(0);
            JSONObject summaryJsonObj3 = routesJsonObj3.getJSONObject("summary");
            JSONArray geometryJsonArray3 = routesJsonObj3.getJSONArray("geometry");
            JSONObject steepnessJsonObj3 = routesJsonObj3.getJSONObject("extras").getJSONObject("steepness");


            JSONArray routesJsonArray4 = ebikeStationToEndpoint.getResponseJSON().getJSONArray("routes");
            JSONObject routesJsonObj4 = routesJsonArray4.getJSONObject(0);
            JSONObject summaryJsonObj4 = routesJsonObj4.getJSONObject("summary");
            JSONArray geometryJsonArray4 = routesJsonObj4.getJSONArray("geometry");

            Double fullDistance = summaryJsonObj1.getDouble("distance") + summaryJsonObj3.getDouble("distance") + summaryJsonObj4.getDouble("distance");
            Double fullDuration = summaryJsonObj1.getDouble("duration") + summaryJsonObj3.getDouble("duration") + summaryJsonObj4.getDouble("duration");

            Double priceDistance = summaryJsonObj3.getDouble("distance");
            JSONObject fullSummaryJson = new JSONObject();
            DistanceCost distanceCost = new DistanceCost();
            fullSummaryJson.put("distance", fullDistance)
                    .put("duration", fullDuration)
                    .put("preference", "fullpath")
                    .put("profile", "cycling-regular")
                    .put("ascent", summaryJsonObj3.getDouble("ascent"))
                    .put("descent", summaryJsonObj3.getDouble("descent"))
                    .put("safe", false)
                    .put("price", distanceCost.CalculationOfCost(priceDistance.intValue(), 5000, 0.1));

            JSONObject fullGeometryJson = new JSONObject();

            fullGeometryJson.put("walkingStart",geometryJsonArray1)
                    .put("cycling",geometryJsonArray3)
                    .put("walkingEnd",geometryJsonArray4);

            JSONObject fullPathSteepnessJson = new JSONObject();
            fullPathSteepnessJson.put("summary", steepnessJsonObj3.getJSONArray("summary"));

            JSONObject fullRouteJson = new JSONObject();
            fullRouteJson.put("summary", fullSummaryJson)
                    .put("geometry", fullGeometryJson)
                    .put("steepness", fullPathSteepnessJson);
            JSONObject fullrouteObj = new JSONObject();
            fullrouteObj.put("route",fullRouteJson );
            routeJsonArray.put(fullrouteObj);

            finalRouteJson.put("routes", routeJsonArray);
            finalRouteJson.put("bbox", bbox);

        }

        return Response.ok(finalRouteJson)
                .status(200)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD")
                .build();
    }
}