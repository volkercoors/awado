package com.api.routes.getEbikeData;

import com.utils.ServiceAccess;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/getEbikeData")
public class GetEbikeData {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getMessage() {
        return Response.ok("SALUT")
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, con" +
                        "tent-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD")
                .build();
    }

    @OPTIONS
    public Response getRequest2() throws JSONException {

        return Response.ok()
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, con" +
                        "tent-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD")
                .build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getRequest(JSONObject object) throws JSONException {

        String eBikeID = object.getString("ebikeID");

        String username = "HftTecUser";
        String password = "rG9F6V11HsAdmYchmr9Wg9nMsyJdVW";

        ServiceAccess service = new ServiceAccess(username, password, true);
        String request =  "https://cvl.daimler-tss.com/icity/vss/bikes/" + eBikeID;
        //String request =  "https://cvl.daimler-tss.com/icity/vss/bikes/eBike201206220009";

        JSONObject ebikeDetails = service.requestObject(request);
        JSONObject response = new JSONObject();
        response.put("lat", ebikeDetails.getJSONObject("geo").getDouble("latitude"));
        response.put("lng", ebikeDetails.getJSONObject("geo").getDouble("longitude"));
        response.put("batteryLevel", ebikeDetails.getInt("fuelLevel"));
        return Response.ok(response)
                .status(200)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD")
                .build();
    }


}