package com.api.routes.getNearestBikes;

import com.bikes.BikeDetails;
import com.bikes.Computing;
import com.bikes.FloatingBike;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/getNearestBikes")
public class GetNearestBikes {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getMessage() {
        return Response.ok("SALUT")
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, con" +
                        "tent-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD")
                .build();
    }

    @OPTIONS
    public Response getRequest2() throws JSONException {

        return Response.ok()
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, con" +
                        "tent-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD")
                .build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getRequest(JSONObject request) throws JSONException {
        JSONObject startpoint = request.getJSONObject("startpoint");

        double lat = startpoint.getDouble("lat");
        double lng = startpoint.getDouble("lng");
        BikeDetails info = new BikeDetails();
        Computing cp = new Computing();

        ArrayList<FloatingBike> ClosestBikes = new ArrayList<FloatingBike>();
        ClosestBikes = cp.CheckForClosestBike(lat, lng, info);
        System.out.println(ClosestBikes);
        JSONArray jsonArray= new JSONArray();
        for (int i = 0; i < ClosestBikes.size(); i++){
            JSONObject jsonObject = new JSONObject();
            System.out.println(ClosestBikes.get(i).getLat());
            System.out.println(ClosestBikes.get(i).getLng());
            System.out.println(ClosestBikes.get(i).getBatteryLevel());
            System.out.println(ClosestBikes.get(i).geteBikeId());
            System.out.println(ClosestBikes.get(i).getDistance());
            if (ClosestBikes.get(i).getDistance() < 2.0) {
                jsonObject.put("id", ClosestBikes.get(i).geteBikeId());
                jsonObject.put("lat", ClosestBikes.get(i).getLat());
                jsonObject.put("lng", ClosestBikes.get(i).getLng());
                jsonObject.put("batteryLevel", ClosestBikes.get(i).getBatteryLevel());
                jsonObject.put("distance", ClosestBikes.get(i).getDistance());
                jsonArray.put(jsonObject);

            }
        }
        return Response.ok(jsonArray)
                .status(200)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD")
                .build();
    }
}