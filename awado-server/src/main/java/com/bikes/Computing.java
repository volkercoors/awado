package com.bikes;

import java.util.ArrayList;

public class Computing {

    private static final double R = 6372.8;

    public static double RetrieveDistance(double lat1, double lon1,
                                  double lat2, double lon2) {

        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(dLat / 2), 2) + Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    }

    public ArrayList<FloatingBike> CheckForClosestBike(double lat, double lng, BikeDetails info){
        info.create();

        for (int i = 0; i < info.ebikes.size(); i++){

            double dist = RetrieveDistance(info.ebikes.get(i).getLat(), info.ebikes.get(i).getLng(), lat, lng);
            info.ebikes.get(i).setDistance(dist);

        }

        ArrayList<Info> bikes = ComputeThreeShortest(info.ebikes);
        ArrayList<FloatingBike> ClosestBikes = new ArrayList<FloatingBike>();
        for (int j = 0; j < bikes.size(); j++){
            for (int i = 0; i < info.ebikes.size(); i++){
                if (info.ebikes.get(i).geteBikeId().equals(bikes.get(j).getId())){
                    ClosestBikes.add(info.ebikes.get(i));
                }
            }
        }
        return ClosestBikes;
    }

    public int sortList(ArrayList<Info> infos){

        int index = -1;
        double tempDist = 0;

        for(int i = 0; i < infos.size(); i++){
            if (infos.get(i).getDistance() > tempDist){
                tempDist = infos.get(i).getDistance();
                index = i;
            }
        }

        return index;
    }

    public ArrayList<Info> ComputeThreeShortest(ArrayList<FloatingBike> bikes){
        ArrayList<Info> close = new ArrayList<Info>(3);

        for (int i = 0; i < bikes.size(); i++) {
            System.out.println("Bikes values is " + bikes.get(i).getDistance() + "Bike id is " + bikes.get(i).geteBikeId());
            double tmpDist = bikes.get(i).getDistance();
            if (i < 3) {
                close.add(new Info(bikes.get(i).geteBikeId(), bikes.get(i).getDistance()));
            } else {
                int index = sortList(close);
                if (bikes.get(i).getDistance() < close.get(index).getDistance()) {
                    close.remove(index);
                    close.add(new Info(bikes.get(i).geteBikeId(), bikes.get(i).getDistance()));
                }
            }
        }

        for (int i = 0; i < close.size(); i++){
            System.out.println(close.get(i).getDistance());
        }

        return close;
    }

    public static void main(String[] args){
        BikeDetails info = new BikeDetails();
        Computing cp = new Computing();

        cp.CheckForClosestBike(48.771264, 9.173164, info);

    }
}
