package com.bikes;


public class FloatingBike {
	private int batteryLevel;
	private String eBikeId;
	private double lng;
	private double lat;
	private double distance;

	public FloatingBike() {	}

	public FloatingBike(int batteryLevel, String eBikeId, double lat, double lng) {
		super();
		this.batteryLevel = batteryLevel;
		this.eBikeId = eBikeId;
		this.lng = lng;
		this.lat = lat;
		this.distance = 0;
	}

	public int getBatteryLevel() {
		return batteryLevel;
	}

	public void setBatteryLevel(int batteryLevel) {
		this.batteryLevel = batteryLevel;
	}

	public String geteBikeId() {
		return eBikeId;
	}

	public void seteBikeId(String eBikeId) {
		this.eBikeId = eBikeId;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public void setDistance(double distance){ this.distance = distance;}

	public double getDistance(){ return this.distance;}
}
	
