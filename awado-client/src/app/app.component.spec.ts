import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { InputsComponent } from './inputs/inputs.component';
import { DirectionsComponent } from './directions/directions.component';
import { MapComponent } from './map/map.component';
import { BrowserModule } from '@angular/platform-browser';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule, MatExpansionModule, MatSnackBarModule, MatRadioModule, MatCheckboxModule } from '@angular/material';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { RouteOverviewComponent } from './directions/route-overview/route-overview.component';
import { APP_BASE_HREF } from '@angular/common';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, InputsComponent, DirectionsComponent, MapComponent, RouteOverviewComponent
      ],
      imports: [
        BrowserModule,
        LeafletModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatSelectModule,
        MatExpansionModule,
        ScrollDispatchModule,
        MatSnackBarModule,
        MatRadioModule,
        MatCheckboxModule,
        NgxChartsModule,
    
      ],
      providers: [{provide: APP_BASE_HREF, useValue: '/'}]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

});
