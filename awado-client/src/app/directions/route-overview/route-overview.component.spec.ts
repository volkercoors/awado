import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteOverviewComponent } from './route-overview.component';
import { DataService } from 'src/app/common/services/data.service';
import { ChangeDetectorRef } from '@angular/core';
import { MatExpansionModule } from '@angular/material';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GetCoordinatesRoute, GetCoordinatesSummary, GetCoordinatesGeometry } from 'src/app/common/classes/get-coordinates-result';

describe('RouteOverviewComponent', () => {
  let component: RouteOverviewComponent;
  let fixture: ComponentFixture<RouteOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatExpansionModule, NgxChartsModule, BrowserAnimationsModule],
      declarations: [ RouteOverviewComponent ],
      providers: [DataService, ChangeDetectorRef] 
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteOverviewComponent);
    component = fixture.componentInstance;
    component.route = new GetCoordinatesRoute();
    component.route.summary = new GetCoordinatesSummary();
    component.route.geometry = new GetCoordinatesGeometry();
    component.route.geometry.walkingStart = [];
    fixture.detectChanges();
  });

  it('should create',() => {
    
    expect(component).toBeTruthy();
  });
});
