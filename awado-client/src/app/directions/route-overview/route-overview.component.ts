import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { GetCoordinatesRoute } from 'src/app/common/classes/get-coordinates-result';
import { DataService } from 'src/app/common/services/data.service';
import { ChartHeightData, DataSet, Step } from 'src/app/common/classes/chart-height-data';

@Component({
  selector: 'app-route-overview',
  templateUrl: './route-overview.component.html',
  styleUrls: ['./route-overview.component.css']
})
export class RouteOverviewComponent implements OnInit {
  @Input() route: GetCoordinatesRoute;
  chosenRoute: GetCoordinatesRoute;
  color: string;
  public panelOpenState: Boolean;
  public heightData: ChartHeightData = new ChartHeightData();
  public customColors = [
    {
      name: 'walking',
      value: '#5AA454'
    },
    {
      name: 'cycling',
      value: '#A10A28'
    }
  ];// these are the colors for the chart

  constructor(private dataService: DataService, private changeDetector: ChangeDetectorRef) { }

  ngOnInit() {
    this.color = this.routeTypeToColor(this.route.summary.preference);
    this.dataService.getChosenRoute().subscribe(route => {
      this.chosenRoute = route;
      this.panelOpenState = this.isChosen() ? true : false
      if (this.panelOpenState) console.info(route.summary.preference);
      this.changeDetector.detectChanges(); //this is needed due to a bug with [expanded] in mat-expansion-panel!
    })
    this.heightData.dataSet[0] = new DataSet();
    this.heightData.dataSet[0].name = "walking"
    this.route.geometry.walkingStart.forEach((height, index) => {
      var step = new Step();
      step.name = "Walking Step " + index + ": " + height[0].toString() + ", " + height[1].toString();
      step.value = height[2];
      this.heightData.dataSet[0].series.push(step)
    })
    if (this.route.geometry.cycling) {
      this.heightData.dataSet[1] = new DataSet();
      this.heightData.dataSet[1].name = "cycling"
      this.route.geometry.cycling.forEach((height, index) => {
        var step = new Step();
        step.name = "Cycling Step " + index + ": " + height[0].toString() + ", " + height[1].toString();
        step.value = height[2];
        this.heightData.dataSet[1].series.push(step)
      })
    }

    if (this.route.geometry.walkingEnd) {
      this.heightData.dataSet[2] = new DataSet();
      this.heightData.dataSet[2].name = "walking"
      this.route.geometry.walkingEnd.forEach((height, index) => {
        var step = new Step();
        step.name = "WalkingEnd Step " + index + ": " + height[0].toString() + ", " + height[1].toString();
        step.value = height[2];
        this.heightData.dataSet[2].series.push(step)
      })
    }
  }
  
  private routeTypeToColor(routeType): string {
    switch (routeType) {
      case 'recommended': return "green";
      case 'fullpath': return "blue";
      case 'shortest': return "red";
      case 'fastest': return "yellow"
      default: return "black"; //happens when server decides to send another preference name.
    }
  }

  public setPanelOpenState(open: boolean){
    if(open) this.dataService.setChosenRoute(this.route);
  }

  public isChosen():boolean{
    return this.route == this.chosenRoute;
  }

}
