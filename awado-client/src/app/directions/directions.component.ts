import { Component, OnInit } from '@angular/core';
import { DataService } from '../common/services/data.service';
import { GetCoordinatesResult, GetCoordinatesRoute } from '../common/classes/get-coordinates-result';

@Component({
  selector: 'app-directions',
  templateUrl: './directions.component.html',
  styleUrls: ['./directions.component.css']
})
export class DirectionsComponent implements OnInit {
  routes: GetCoordinatesResult
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getRoute().subscribe(result => {
      this.routes = result;
    })
  }
}
