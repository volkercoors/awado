import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectionsComponent } from './directions.component';
import { RouteOverviewComponent } from './route-overview/route-overview.component';
import { DataService } from '../common/services/data.service';
import { MatExpansionModule } from '@angular/material';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('DirectionsComponent', () => {
  let component: DirectionsComponent;
  let fixture: ComponentFixture<DirectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectionsComponent, RouteOverviewComponent ],
      providers: [DataService],
      imports: [MatExpansionModule, NgxChartsModule, BrowserAnimationsModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
