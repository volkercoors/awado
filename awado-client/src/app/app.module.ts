import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { InputsComponent } from './inputs/inputs.component';
import { DataService } from './common/services/data.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { DirectionsComponent } from './directions/directions.component';
import { MessageComponent } from './message/message.component';
import { MatSnackBarModule, MatRadioModule, MatCheckboxModule } from '@angular/material';
import { RouteOverviewComponent } from './directions/route-overview/route-overview.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TruncatePipe } from './common/pipes/truncate.pipe';
@NgModule({
  entryComponents: [MessageComponent],

  declarations: [
    AppComponent,
    MapComponent,
    InputsComponent,
    DirectionsComponent,
    MessageComponent,
    RouteOverviewComponent,
    TruncatePipe
  ],
  imports: [
    BrowserModule,
    LeafletModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatExpansionModule,
    ScrollDispatchModule,
    MatSnackBarModule,
    MatRadioModule,
    MatCheckboxModule,
    NgxChartsModule

  ],
  providers: [DataService], //dataservice is here because it is a singleton
  bootstrap: [AppComponent]
})
export class AppModule { }
