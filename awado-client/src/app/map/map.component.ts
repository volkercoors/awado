import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { tileLayer, latLng, Map, marker, icon, Polyline, Marker, LatLng, LayerGroup, layerGroup } from 'leaflet';
import { DataService } from '../common/services/data.service';
import { Ebike } from '../common/classes/ebike';
import { GetCoordinatesResult } from '../common/classes/get-coordinates-result';
import { MatSnackBar } from '@angular/material';
import { MessageComponent } from '../message/message.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit {
  
  constructor(private dataService: DataService, private changeDetector: ChangeDetectorRef, public snackBar: MatSnackBar) {}

  routes: Polyline[]=[];
  startDestMarkers: Marker[];
  bikeMarkers: Marker[];
  markersGroup: LayerGroup = layerGroup();
  routeMarkersGroup: LayerGroup = layerGroup();
  bikeMarkersGroup: LayerGroup = layerGroup();
  routeGroup: LayerGroup = layerGroup();
  map: Map;

  // Define our base layers so we can reference them where needed
  streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    detectRetina: true,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });
  wMaps = tileLayer('http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
    detectRetina: true,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });
  cycleMaps = tileLayer('http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png', {
    detectRetina: true,
    attribution: '&copy; <a href="http://www.opencyclemap.org/copyright">OpenCycleMap</a> contributors - &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  });

    layersControl = {
      baseLayers: {
        'Open Street Maps': this.streetMaps,
        'Wikimedia Maps': this.wMaps,
        'Cycle Maps' : this.cycleMaps
      },
      overlays: {
        'Route': this.routeGroup,
        'Markers' : this.markersGroup,
        'Route Markers' : this.routeMarkersGroup,
        'Bike Markers' : this.bikeMarkersGroup
      }
    };
  options = {
    layers: [ this.streetMaps, this.markersGroup, this.routeGroup, this.routeMarkersGroup, this.bikeMarkersGroup ],
    zoom: 10,
    center: latLng(48.7758459,9.1829321)
  };

  ngOnInit() {
    this.refreshRoute();
    this.setStartDestMarkers();
    this.dataService.getChooseLocationsOnMap().subscribe(() => this.refreshStartDestMarkers(this.dataService.startCoord, this.dataService.destCoord))
    this.startDestMarkers[0].on('move', (event)=> {
      this.onMarkerMoved(true, event)
    })
    this.startDestMarkers[1].on('move', (event)=> {
      this.onMarkerMoved(false, event)
    })
    this.dataService.getNearestBikes().subscribe(bikes => this.refreshBikeLocations(bikes))
    this.ChosenRouteSubscribe();
  }

  private ChosenRouteSubscribe(){
    this.dataService.getChosenRoute().subscribe(route => {
      
      this.routes.forEach(routepart => routepart.setStyle({
        className: "notChosenRoute",
        color: "grey",
      }))
      this.routes.filter(layer => (layer.getTooltip().getContent()==route.summary.preference)).forEach(routepart =>
        { 
          var color = this.routeTypeToColor(routepart.getTooltip().getContent())
          routepart.setStyle({
            className: "chosenRoute",
            color: color,
          })
          routepart.bringToFront();
        });
    })
  }

  private routeTypeToColor(routeType) : string{
    switch(routeType){
      case 'recommended': return "green";
      case 'fullpath': return "blue";
      case 'shortest': return "red";
      case 'fastest': return "yellow"
      default: return "black"; //shouldnt happen
    }
  }

  private refreshBikeLocations(bikes: Ebike[]){
    this.bikeMarkers = [];
    this.bikeMarkersGroup.clearLayers();
    bikes.forEach(bike =>{
      var location = latLng(bike.lat, bike.lng)
      var batteryLevel= bike.batteryLevel

     
      var newmarker = marker(location, {
        icon: icon({
          iconSize: [ 40, 40 ],
          iconAnchor: [ 20, 40 ],
          iconUrl: 'assets/ebike.png',
          //shadowUrl: 'assets/marker-shadow.png'
        }),
          title: "ebike",
          riseOnHover: true,
          draggable: false,
      }).bindTooltip("Ebike" + bike.id + " Battery " + bike.batteryLevel + "%")
      this.bikeMarkers.push(newmarker);
      this.bikeMarkersGroup.addLayer(newmarker);
      newmarker.on('click', ()=> {
        this.dataService.chosenEbike = bike;
        
        this.bikeMarkers.forEach(marker => {
          marker.setIcon(
            icon({
              iconSize: [ 40, 40 ],
              iconAnchor: [ 20, 40 ],
              iconUrl: 'assets/ebike.png'})
          )})
        newmarker.setIcon(icon({
          iconSize: [ 60, 60 ],
          iconAnchor: [ 30, 60 ],
          iconUrl: 'assets/ebike-chosen.png'}))
          console.info("battery", bike.batteryLevel)
          {if (bike.batteryLevel < 15)
            { "openSnackBar()"; {
             this. snackBar.openFromComponent(MessageComponent, {
               duration: 500, 
             });
           }}
           };
      })
    })
  }

  onMarkerMoved(isStart, event){
    if(latLng){
    if(isStart){
      this.dataService.startCoord = event.latlng;
    }
      else {
        this.dataService.destCoord = event.latlng;
      }
    this.dataService.setMarkersMoved(true);
    }
  }

  public getStartDestMarkers(): Marker[]{
    return [marker(this.dataService.startCoord, {
      icon: icon({
        iconSize: [ 40, 40 ],
        iconAnchor: [ 15, 40 ],
        iconUrl: 'assets/marker-icon_red.png',
        //shadowUrl: 'assets/marker-shadow.png'
      }),
        title: "start marker",
        riseOnHover: true,
        draggable: true
    }).bindTooltip("Start"),
    marker(this.dataService.destCoord, {
      icon: icon({
        iconSize: [ 40, 40 ],
        iconAnchor: [ 15, 40 ],
        iconUrl: 'assets/marker-icon_red.png',
        //shadowUrl: 'assets/marker-shadow.png'
      }), 
        title: "destination marker",
        riseOnHover: true,
        draggable: true,
    }).bindTooltip("Destination")
  ];
  }

  refreshRoute(){
    
    // the route comes from the service. This method will be called when a new route is available.
    // It subscribes to the observable from the server, meaning it is ASYNC
    // => the page can load immediately, and this element will be added whenever it is received.
    this.dataService.getRoute().subscribe((result: GetCoordinatesResult)=> {
      //cleanup old routes
      this.routes=[];
      this.routeGroup.clearLayers();
      result.routes.forEach(route =>{
        
        var geom = route.route.geometry;

        var walkingStart = new Polyline([[geom.walkingStart[0][1], geom.walkingStart[0][0]]], { color: 'grey'} )
        .bindTooltip(route.route.summary.preference).setStyle({
          dashArray: "5",
          weight: 4
        })
        geom.walkingStart.forEach(point => {  
          walkingStart.addLatLng(latLng(point[1], point[0]));
        });
        this.routeGroup.addLayer(walkingStart);
        this.routes.push(walkingStart);
        walkingStart.on('click', ()=> {
          this.dataService.setChosenRoute(route.route);
        })

        if(geom.cycling){
        var cycling = new Polyline([[geom.cycling[0][1], geom.cycling[0][0]]], { color: 'grey'} )
        .bindTooltip(route.route.summary.preference).setStyle({
          weight: 4
        })
        geom.cycling.forEach(point => {  
          cycling.addLatLng(latLng(point[1], point[0]));
        });
        this.routeGroup.addLayer(cycling);
        this.routes.push(cycling);
        cycling.on('click', ()=> {
          this.dataService.setChosenRoute(route.route);
        })
      }

        if(geom.walkingEnd){
        var walkingEnd = new Polyline([[geom.walkingEnd[0][1], geom.walkingEnd[0][0]]], { color: 'grey'} )
        .bindTooltip(route.route.summary.preference).setStyle({
          dashArray: "5",
          weight: 4
        })
        geom.walkingEnd.forEach(point => {  
          walkingEnd.addLatLng(latLng(point[1], point[0]));
        });
        this.routeGroup.addLayer(walkingEnd);
        this.routes.push(walkingEnd);
        walkingEnd.on('click', ()=> {
          this.dataService.setChosenRoute(route.route);
        })
      }
      this.ChosenRouteSubscribe();
      
      })
      this.map.fitBounds([[result.bbox[1],result.bbox[0]], [result.bbox[3],result.bbox[2]]]);
    }, 
    error => console.log("Error getting route", error) )
  }

  setStartDestMarkers(){
    this.startDestMarkers = this.getStartDestMarkers();
    this.routeMarkersGroup.addLayer(this.startDestMarkers[0])
    this.routeMarkersGroup.addLayer(this.startDestMarkers[1])
  }

  refreshStartDestMarkers(start: LatLng, dest: LatLng){
    this.startDestMarkers[0].setLatLng(start)
    this.startDestMarkers[1].setLatLng(dest)
    this.changeDetector.detectChanges();
  }

  onMapReady(map: Map) {
    this.map = map;
    map.setView(this.options.center,10);
  }

}
