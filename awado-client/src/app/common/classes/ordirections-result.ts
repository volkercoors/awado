export class ORDirectionsResult {
    public bbox: number[];
    public routes: ORDirectionsRoute[]
}

export class ORDirectionsRoute {
    public bbox: number[];
    public summary: ORDirectionsSummary;
    public geometry: number[][];
    public segments: ORDirectionsSegment[];
}

export class ORDirectionsSummary {
    public distance: number;
    public duration: number;
}

export class ORDirectionsSegment {
    public distance: number;
    public duration: number;
    public steps: ORDirectionsStep[];
}

export class ORDirectionsStep {
    public distance: number;
    public duration: number;
    public type: number;
    public instruction: string;
    public name: string;
    public way_points: number[]; //always 2, start and end of it, refer to array positions in the geometry (if we need this, check that server does start at the right number when adding segments together)
}
