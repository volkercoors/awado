export class GetCoordinatesResult {
    public routes: GetCoordinatesRoutesContainer[];
    public bbox: number[];
}

export class GetCoordinatesRoutesContainer {
    public route: GetCoordinatesRoute;
}

export class GetCoordinatesRoute {
    public summary: GetCoordinatesSummary;
    public geometry: GetCoordinatesGeometry;
}

export class GetCoordinatesGeometry {
    walkingStart: number[][];
    cycling?: number[][];
    walkingEnd?: number[][];
}

export class GetCoordinatesSummary {
    public distance: number;
    public duration: number;
    public ascent: number;
    public descent: number;
    public preference: string;
    public price: number;
}




