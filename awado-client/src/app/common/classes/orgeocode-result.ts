
export class ORGeocodeResult {
    public geocoding;
    public features: ORGeocodeFeature[];
}
export class ORGeocodeFeature {
    public type: string;
    public geometry: ORGeocodeGeometry
}

export class ORGeocodeGeometry {
    public type: string;
    public coordinates: number[];
}