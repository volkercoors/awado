export class ChartHeightData {
    public dataSet: DataSet[] = []
}

export class DataSet {
    public name: String;
    public series: Step[] = []
}

export class Step {
    public name: String;
    public value: number
}