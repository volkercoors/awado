import { LatLng } from "leaflet";

export class AwadoServerRoute {
    public coordinates: LatLng[];
}

export class AwadoCoordinates {
    constructor(
    public startpoint: LatLng,
    public endpoint: LatLng
    )
    {}
}
