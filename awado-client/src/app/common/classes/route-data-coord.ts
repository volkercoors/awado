import { LatLng} from "leaflet";

export class RouteDataCoord {


    constructor(
        public start: LatLng,
        public dest: LatLng) {
    }

    toString(): string {
        return "route from (" + this.start.lat + ", " + this.start.lng + ") to (" + this.dest.lat + ", " + this.dest.lng + ")"
    }

}
