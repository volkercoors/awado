export class Ebike {
    public id: number;
    public lat: number;
    public lng: number;
    public batteryLevel: number;
}
