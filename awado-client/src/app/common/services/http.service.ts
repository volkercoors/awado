import { Injectable } from '@angular/core';
import { LatLng } from 'leaflet';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ORGeocodeResult } from '../classes/orgeocode-result';
import { ORDirectionsResult } from '../classes/ordirections-result';
import { Ebike } from '../classes/ebike';
import { GetCoordinatesResult } from '../classes/get-coordinates-result';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private http: HttpClient) { }

  public getLatlngOR(query: string): Observable<ORGeocodeResult>{
    return this.http.get<ORGeocodeResult>("https://api.openrouteservice.org/geocode/search?api_key=5b3ce3597851110001cf624856b86ef9801c4696912904857f046f8e&text="+query); //This key is Silvies key. should switch key to users key?
  }

  public getRouteOR(coordsStart: LatLng, coordsDest: LatLng): Observable<ORDirectionsResult>{
    var connString = "https://api.openrouteservice.org/directions?api_key=5b3ce3597851110001cf624856b86ef9801c4696912904857f046f8e"+
    "&coordinates="+coordsStart.lng+","+coordsStart.lat+"%7C"+ coordsDest.lng+","+coordsDest.lat+"&profile=cycling-regular&geometry_format=polyline&instructions=true" //cycling-safe does not work
    return this.http.get<ORDirectionsResult>(connString);
  } //Not used currently

  public getNearestBikes(coords: LatLng): Observable<Ebike[]>{
    return this.http.post<Ebike[]>("http://vm11.fkc.hft-stuttgart.de:8080/awado-server_war/getNearestBikes", {"startpoint": coords});
  }
  
  public getRouteAwado(startPoint: LatLng, endPoint: LatLng, ebikeID: number){
    return this.http.post<GetCoordinatesResult>("http://vm11.fkc.hft-stuttgart.de:8080/awado-server_war/getCoordinates", {"startpoint": startPoint, "endpoint": endPoint, "ebikeID": ebikeID});
}
}
