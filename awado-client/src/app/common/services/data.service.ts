import { Injectable } from '@angular/core';
import { LatLng, latLng } from 'leaflet';
import { Subject } from 'rxjs';
import { Ebike } from '../classes/ebike';
import { GetCoordinatesRoute, GetCoordinatesResult } from '../classes/get-coordinates-result';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  startCoord: LatLng = latLng(48.6, 9.1)
  destCoord: LatLng = latLng(48.5, 10.1)
  private ChooseLocationsOnMap = new Subject<boolean>(); //Throwing events when lat/long changed in interface
  private MarkersMoved = new Subject<boolean>(); //throwing events when markers moved
  public route = new Subject<GetCoordinatesResult>(); //throwing events when route has changed
  public chosenRoute = new Subject<GetCoordinatesRoute>(); //throwing events when user chose a route
  public nearestEbikes = new Subject<Ebike[]>(); //throwing events when nearestEbikes were loaded
  public chosenEbike: Ebike; //this can be stored simply, as theres no component having to react to it atm
  constructor() {
  }

  public setRoute(newVal: GetCoordinatesResult) {
    this.route.next(newVal);
  }

  public getRoute() {
    return this.route.asObservable();
  }
  public setChosenRoute(newVal: GetCoordinatesRoute) {
    this.chosenRoute.next(newVal);
  }

  public getChosenRoute() {
    return this.chosenRoute.asObservable();
  }

  public getChooseLocationsOnMap() {
    return this.ChooseLocationsOnMap.asObservable();
  }

  public setChooseLocationsOnMap(newVal: boolean) {
    this.ChooseLocationsOnMap.next(newVal);
  }

  public getMarkersMoved() {
    return this.MarkersMoved.asObservable();
  }

  public setMarkersMoved(newVal: boolean) {
    this.MarkersMoved.next(newVal);
  }

  public setNearestBikes(newVal: Ebike[]) {
    this.nearestEbikes.next(newVal);
  }

  public getNearestBikes() {
    return this.nearestEbikes.asObservable();
  }
}
