import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {

  //"magic" numbers should actually be args to the pipe, but this somehow didnt work
  transform(value: string, args: string[]): string {
    if(value){
    const limit = args.length > 0 ? parseInt(args[0], 10) : 8;
    const cut = args.length > 1 ? parseInt(args[1], 10) : 6;
    const trail = args.length > 2 ? args[2] : '.';
    return value.length > limit ? value.substring(0, cut) + trail : value;
    }
  }

}
