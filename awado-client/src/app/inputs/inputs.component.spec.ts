import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputsComponent } from './inputs.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MatRadioModule, MatCheckboxModule, MatSelectModule, MatSnackBar } from '@angular/material';
import { ChangeDetectorRef } from '@angular/core';
import { HttpService } from '../common/services/http.service';
import { DataService } from '../common/services/data.service';
import { HttpClient, HttpHandler } from '@angular/common/http';


describe('InputsComponent', () => {
  let component: InputsComponent;
  let fixture: ComponentFixture<InputsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule, FormsModule, MatRadioModule, MatCheckboxModule, MatSelectModule],
      providers: [DataService, ChangeDetectorRef, HttpService, MatSnackBar, HttpClient, HttpHandler],
      declarations: [ InputsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
