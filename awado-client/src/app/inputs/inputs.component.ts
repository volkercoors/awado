import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RouteDataString } from '../common/classes/route-data-string';
import { RouteDataCoord } from '../common/classes/route-data-coord';
import { latLng } from 'leaflet';
import { DataService } from '../common/services/data.service';
import { HttpService } from '../common/services/http.service';
import { ORGeocodeResult, ORGeocodeFeature } from '../common/classes/orgeocode-result';
import { bike_type } from '../common/classes/bike_type';
import { Ebike } from '../common/classes/ebike';
import { MatSnackBar } from '@angular/material';
import { MessageComponent } from '../message/message.component';
import { GetCoordinatesResult } from '../common/classes/get-coordinates-result';
import { RadioNgModel } from '../common/classes/radio-ng-model';

//import { empty } from 'rxjs';
//import { isNull } from '@angular/compiler/src/output/output_ast';
//import { MatSnackBarRef } from '@angular/material'; 
//import { SimpleSnackBar} from '@angular/material';
@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.css']
})
export class InputsComponent implements OnInit {

  model = new RouteDataString("HFT Stuttgart", "Stuttgart HBF"); //using two way data binding here - the model will change when the input fields change and vice versa

  modelCoord = new RouteDataCoord(latLng(this.dataService.startCoord.lat, this.dataService.startCoord.lng), latLng(this.dataService.destCoord.lat, this.dataService.destCoord.lng))
  asString = true; //this is for switching out the string and the lat/long inputs
  numberMatcher = "^[0-9]+(\.[0-9]+)?$"
  startLocationFeatures: ORGeocodeFeature[]
  destLocationFeatures: ORGeocodeFeature[]
  selectedStart: ORGeocodeFeature
  selectedDest: ORGeocodeFeature
  startError
  destError

  bike_type: bike_type[];

  trans: RadioNgModel = new RadioNgModel()
  transSelected: String = 'e-bike'

  disableFlagGetRouteButton: Boolean = false
  disableFlagGetNearestButton: Boolean = false

  constructor(private dataService: DataService, private changeDetector: ChangeDetectorRef, private httpService: HttpService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.dataService.getMarkersMoved().subscribe(() => this.refreshValues())
  }

  refreshValues() {
    this.modelCoord.start.lat = this.dataService.startCoord.lat
    this.modelCoord.start.lng = this.dataService.startCoord.lng
    this.modelCoord.dest.lat = this.dataService.destCoord.lat
    this.modelCoord.dest.lng = this.dataService.destCoord.lng
    this.changeDetector.detectChanges();
  }

 switchStringCoord() {
  this.asString = !this.asString;
  this.dataService.setChooseLocationsOnMap(false);
}

moveMarker() {
  if (!this.modelCoord.start.lat.toString().match(this.numberMatcher) ||
    !this.modelCoord.start.lng.toString().match(this.numberMatcher) ||
    !this.modelCoord.dest.lat.toString().match(this.numberMatcher) ||
    !this.modelCoord.dest.lng.toString().match(this.numberMatcher)) {
    this.refreshValues();
  }
  else {
    this.dataService.startCoord = this.modelCoord.start;
    this.dataService.destCoord = this.modelCoord.dest;
    this.dataService.setChooseLocationsOnMap(true);

  }
}

onStartBlur() {
  this.httpService.getLatlngOR(this.model.start).subscribe((value: ORGeocodeResult) => {
    if (value.features[0]) {
      this.modelCoord.start.lat = value.features[0].geometry.coordinates[1]; //the openroute service uses long/lat!!
      this.modelCoord.start.lng = value.features[0].geometry.coordinates[0];
      this.startLocationFeatures = value.features;
      this.selectedStart = value.features[0];
      this.moveMarker();
      this.startError = null;
    }
    else {
    this.startError = "Does not exist";
      console.log(this.startError)
    }
  },
    error => {
    this.startError = error;
      console.log(error);
    });
}
onStartSelectionChange(event) {
  console.log(event)
  this.modelCoord.start.lat = event.value.geometry.coordinates[1];
  this.modelCoord.start.lng = event.value.geometry.coordinates[0];
  this.moveMarker();
}

onDestBlur() {
  this.httpService.getLatlngOR(this.model.destination).subscribe((value: ORGeocodeResult) => {
    if (value.features[0]) {
      this.modelCoord.dest.lat = value.features[0].geometry.coordinates[1];
      this.modelCoord.dest.lng = value.features[0].geometry.coordinates[0];
      this.destLocationFeatures = value.features;
      this.selectedDest = value.features[0];
      this.moveMarker();
      this.destError = null;
    }
    else this.destError = "Does not exist"
  },
    error => this.destError = error);
}
onDestSelectionChange(event) {
  this.modelCoord.dest.lat = event.value.geometry.coordinates[1];
  this.modelCoord.dest.lng = event.value.geometry.coordinates[0];
  this.moveMarker();
}

onSubmit() {
var bikeId = this.dataService.chosenEbike?this.dataService.chosenEbike.id : -1;
    this.httpService.getRouteAwado(this.dataService.startCoord, this.dataService.destCoord, bikeId).subscribe((value: GetCoordinatesResult) => {
      if(!value.routes[0].route.summary.preference) value.routes[0].route.summary.preference = "Walking" //to avoid errors as the preference somehow comes null on walking path
    this.dataService.setRoute(value);
    this.dataService.setChosenRoute(value.routes[0].route);
    console.info(value)
  },
    error => console.log("Error getting route", error));
}

onSubmitStart() {
  this.httpService.getNearestBikes(this.dataService.startCoord).subscribe((ebikes: Ebike[]) => {
    this.dataService.setNearestBikes(ebikes);
    this.dataService.chosenEbike = ebikes[0];

    if (ebikes.length == 0 ) {
      this.snackBar.open("No ebikes within distance. Chose another starting point or you will have to walk.", null, {
        duration: 4000,
      });
    } else {
      this.dataService.chosenEbike = ebikes[0];
    }  
  },
    error => console.log("Error getting ebikes", error));
}

radioButtonChanged(option){

switch(option.value){
  case 'e-bike':
    this.disableFlagGetNearestButton = false;
    break;
  
  default:
    this.disableFlagGetNearestButton = true;
    this.dataService.chosenEbike = null;
    this.dataService.setNearestBikes([]);
}
}
}