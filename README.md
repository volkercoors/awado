# AWADO

<!--- ## OGC SensorThings API & E-bike usage data
* What is SensorThings API? Standard Document here: http://www.opengeospatial.org/standards/sensorthings

* Learn how to use it here (Credit: SensorUp):
http://developers.sensorup.com/docs/

* HFT SensorThings API Server: 
http://gisstudio.hft-stuttgart.de/FROST-icity/v1.0

* FROST SensorThings API: https://github.com/FraunhoferIOSB/FROST-Server

* Contact Joe
  * Emai: thunyathep.s@outlook.com
  * Facebook Messenger: Jojoe DS
--->

## Slack for this project:
"https://join.slack.com/t/hft-softwareproject/shared_invite/enQtNDUxMjUwMTEyNDAwLTNlM2RmYWY5ODdkOWIzMGQzNmE3Y2Q4ZGZlNWU3NmY3ZDhhZmQ4YWFjNWZkNTg2MjQ2OGI2ZGRiMjRmODkzNjk"


## Frontend - How to start 
- Install NodeJS https://nodejs.org/en/download/
- Install angularCLI by typing in a cmd: npm install -g @angular/cli
- cd to the project directory, run npm install (this will install all packages)
- run ng serve
- navigate your browser to localhost:4200